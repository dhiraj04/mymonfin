from rest_framework import serializers

from consumers.models import Consumer, GENDER


class ConsumerDetailSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField()

    class Meta:
        model = Consumer
        fields = [
            'id',
            'first_name',
            'last_name',
            'full_name',
            'gender',
            'dob',
            'email',
            'address',
            'mobile'
        ]

    def get_full_name(self, obj):
        return obj.get_full_name()


class ConsumerEditSerializer(serializers.ModelSerializer):
    gender = serializers.ChoiceField(choices=GENDER)

    class Meta:
        model = Consumer
        fields = [
            'id',
            'first_name',
            'last_name',
            'gender',
            'dob',
            'email',
            'address',
            'mobile',
        ]

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.gender = validated_data.get('gender', instance.gender)
        instance.dob = validated_data.get('dob', instance.dob)
        instance.email = validated_data.get('email', instance.email)
        instance.address = validated_data.get('address', instance.address)
        instance.mobile = validated_data.get('mobile', instance.mobile)
        instance.save()

        user = instance.user
        user.first_name = instance.first_name
        user.last_name = instance.last_name
        user.email = instance.email
        user.save()

        return instance
