from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from consumers.models import Consumer
from consumers.serializers import ConsumerDetailSerializer, ConsumerEditSerializer


class ConsumerDetailView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        consumer = Consumer.objects.get(user=self.request.user)
        return consumer

    def get_serializer_class(self):
        return ConsumerDetailSerializer

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset)
        return Response(serializer.data)


class ConsumerEditView(APIView):
    permission_classes = [IsAuthenticated]

    def put(self, request):
        serializer = ConsumerEditSerializer(instance=request.user.consumers, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
