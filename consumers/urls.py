from django.urls import path

from consumers.views import ConsumerDetailView, ConsumerEditView

app_name = 'consumers'

urlpatterns = [
    path('profile/', ConsumerDetailView.as_view(), name='profile'),
    path('profile/edit/', ConsumerEditView.as_view(), name='profile_edit'),
]