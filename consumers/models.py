from django.db import models
from model_utils import Choices

from users.models import User

GENDER = Choices(
    'Male',
    'Female',
    'Other'
)


class Consumer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='consumers')
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    gender = models.CharField(max_length=15, choices=GENDER, null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    email = models.EmailField(unique=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    mobile = models.CharField(max_length=10, null=True, blank=True)
    # avatar = models.ImageField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'consumer'

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        full_name = self.first_name + ' ' + self.last_name
        return full_name
