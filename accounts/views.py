from rest_framework import generics, status, serializers
from rest_framework.exceptions import NotFound
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from accounts.models import AccountGroup, Account
from accounts.serializers import AccountGroupListSerializer, AccountGroupCreateSerializer, AccountGroupEditSerializer, \
    AccountListSerializer, AccountCreateSerializer, AccountEditSerializer, AccountGroupDetailSerializer, \
    TotalAmountSerializer, AccountDetailSerializer
from users.models import User


class AccountGroupListView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        account_group = AccountGroup.objects.filter(user=self.request.user).order_by('-id')
        return account_group

    def get_serializer_class(self):
        return AccountGroupListSerializer

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class AccountGroupCreateView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        user = self.request.user
        serializer = AccountGroupCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=user)

            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AccountGroupDetailView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self, pk):
        try:
            account_group = AccountGroup.objects.get(pk=pk)
        except AccountGroup.DoesNotExist:
            raise NotFound(detail='Account Group Not Found')
        return account_group

    def get_serializer_class(self):
        return AccountGroupDetailSerializer

    def get(self, request, pk):
        queryset = self.get_queryset(pk)
        serializer = self.get_serializer(queryset)
        return Response(serializer.data)


class AccountGroupEditView(APIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self, pk):
        try:
            account_group = AccountGroup.objects.get(pk=pk)
        except AccountGroup.DoesNotExist:
            raise NotFound(detail='Account Group Not Found')
        return account_group

    def get(self, request, pk):
        queryset = self.get_queryset(pk)
        serializer = AccountGroupDetailSerializer(queryset)
        return Response(serializer.data)

    def put(self, request, pk):
        try:
            account_group = AccountGroup.objects.get(pk=pk, user=self.request.user)
        except AccountGroup.DoesNotExist:
            raise serializers.ValidationError({"AccountGroup": "Account Group Not Found"})
        serializer = AccountGroupEditSerializer(instance=account_group, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AccountTotalAmountView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return user

    def get_serializer_class(self):
        return TotalAmountSerializer

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset)
        return Response(serializer.data)


class AccountListView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        account = Account.objects.filter(user=self.request.user).order_by('-id')
        return account

    def get_serializer_class(self):
        return AccountListSerializer

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        # page = self.paginate_queryset(queryset)
        # if page:
        #     serializer = self.get_serializer(page, many=True)
        #     return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class AccountCreateView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        user = request.user
        serializer = AccountCreateSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save(user=user)

            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AccountEditView(APIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self, pk):
        try:
            account = Account.objects.get(pk=pk)
        except Account.DoesNotExist:
            raise NotFound(detail='Account Group Not Found')
        return account

    def get(self, request, pk):
        queryset = self.get_queryset(pk)
        serializer = AccountDetailSerializer(queryset)
        return Response(serializer.data)

    def put(self, request, pk):
        try:
            account = Account.objects.get(pk=pk, user=self.request.user)
        except Account.DoesNotExist:
            raise serializers.ValidationError({"Account": "Account Not Found"})
        serializer = AccountEditSerializer(instance=account, data=request.data, partial=True,
                                           context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
