from rest_framework import serializers
from rest_framework.fields import Field, ReadOnlyField

from accounts.models import AccountGroup, Account
from users.models import User


class AccountGroupListSerializer(serializers.ModelSerializer):

    class Meta:
        model = AccountGroup
        fields = [
            'id',
            'group_name',
        ]


class AccountGroupCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = AccountGroup
        fields = [
            'id',
            'group_name',
        ]

    def create(self, validated_data):
        account_group = AccountGroup.objects.create(**validated_data)
        account_group.save()

        return account_group


class AccountGroupEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = AccountGroup
        fields = [
            'id',
            'group_name'
        ]


class AccountGroupDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = AccountGroup
        fields = [
            'id',
            'group_name'
        ]


class TotalAmountSerializer(serializers.ModelSerializer):
    total_amount = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = [
            'total_amount'
        ]

    def get_total_amount(self, obj):
        return obj.get_total_amount()


class AccountListSerializer(serializers.ModelSerializer):
    account_group = serializers.SerializerMethodField()

    class Meta:
        model = Account
        fields = [
            'id',
            'account_name',
            'amount',
            'account_group',
        ]

    def get_account_group(self, obj):
        return obj.account_group.group_name


class AccountCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Account
        fields = [
            'id',
            'account_name',
            'amount',
            'account_group',
        ]

    def validate(self, data):
        user = self.context['request'].user
        user_acc_group = AccountGroup.objects.filter(user=user)
        account_group_data = data['account_group']
        if account_group_data not in user_acc_group:
            raise serializers.ValidationError({'AccountGroup': 'Account Group Not Found'})
        return super(AccountCreateSerializer, self).validate(data)

    def create(self, validated_data):
        account = Account.objects.create(**validated_data)
        account.save()

        return account


class AccountEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = Account
        fields = [
            'id',
            'account_name',
            'account_group',
        ]

    def validate(self, data):
        user = self.context['request'].user
        user_acc_group = AccountGroup.objects.filter(user=user)
        account_group_data = data['account_group']
        if account_group_data not in user_acc_group:
            raise serializers.ValidationError({'AccountGroup': 'Account Group Not Found'})
        return super(AccountEditSerializer, self).validate(data)

    def update(self, instance, validated_data):
        instance.account_name = validated_data.get('account_name', instance.account_name)
        instance.account_group = validated_data.get('account_group', instance.account_group)
        instance.save()

        return instance


class AccountDetailSerializer(serializers.ModelSerializer):
    account_group = AccountGroupDetailSerializer()

    class Meta:
        model = Account
        fields = [
            'id',
            'account_name',
            'account_group'
        ]
