from django.urls import path
from accounts.views import AccountGroupListView, AccountGroupCreateView, AccountGroupEditView, AccountListView, \
    AccountCreateView, AccountEditView, AccountGroupDetailView, AccountTotalAmountView

app_name = 'accounts'

urlpatterns = [
    path('', AccountListView.as_view(), name='account'),
    path('add/', AccountCreateView.as_view(), name='account_add'),
    path('edit/<int:pk>/', AccountEditView.as_view(), name='account_edit'),

    path('groups/', AccountGroupListView.as_view(), name='group'),
    path('groups/add/', AccountGroupCreateView.as_view(), name='group_add'),
    path('groups/edit/<int:pk>/', AccountGroupEditView.as_view(), name='group_edit'),
    path('groups/detail/<int:pk>/', AccountGroupDetailView.as_view(), name='group_detail'),

    path('total-amount/', AccountTotalAmountView.as_view(), name='total_amount')
]