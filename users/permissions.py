from rest_framework.permissions import BasePermission


class IsAdmin(BasePermission):

    message = "You must be admin to access this endpoint"

    def has_permission(self, request, view):
        return request.user and request.user.is_admin()
