import jwt
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers

from accounts.models import AccountGroup
from consumers.models import Consumer
from mymonfin import settings

User = get_user_model()


class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField(write_only=True)

    def validate(self, attrs):
        credentials = {
            'username': attrs.get('username'),
            'password': attrs.get('password')
        }

        if all(credentials.values()):
            try:
                user = User.objects.get(email=attrs.get('username'))
            except User.DoesNotExist:
                try:
                    user = User.objects.get(username=attrs.get('username'))
                except User.DoesNotExist:
                    try:
                        user = User.objects.get(username=attrs.get('username'))
                    except User.DoesNotExist:
                        msg = 'Invalid Username or Password'
                        raise serializers.ValidationError({"details": msg})

            credentials['username'] = user.email

            user = authenticate(**credentials)

            if user:
                payload = {
                    'user_id': user.pk,
                    'username': user.username,
                    'role': user.role,
                }

                key = settings.SECRET_KEY

                token = jwt.encode(
                    payload,
                    key,
                ).decode('utf-8')

                u = user
                u.mob_token = token
                u.save()

                return {
                    'token': token,
                    'user': user,
                    'role': user.role
                }

            else:
                raise serializers.ValidationError({"details": "Could not login with given credentials"})
        else:
            raise serializers.ValidationError({"details": "Please Provide username and password"})


class UserDetailSerializer(serializers.ModelSerializer):
    username = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'email',
        ]

    def get_username(self, obj):
        return obj.username


class UserRegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    confirm_password = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'username',
            'email',
            'password',
            'confirm_password'
        ]

    def validate(self, data):
        password = data['password']
        confirm_password = data.pop('confirm_password')
        if password != confirm_password:
            raise serializers.ValidationError({"confirm_password": "Password didn't match"})

        return super(UserRegisterSerializer, self).validate(data)

    def create(self, validated_data):
        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()

        consumer = Consumer()
        consumer.user = user
        consumer.first_name = user.first_name
        consumer.last_name = user.last_name
        consumer.email = user.email
        consumer.save()

        AccountGroup.objects.bulk_create([
            AccountGroup(group_name='Bank', user=user),
            AccountGroup(group_name='Cash', user=user),
            AccountGroup(group_name='E-Wallet', user=user)
        ])

        return user


class UserPasswordChangeSerializer(serializers.Serializer):
    password = serializers.CharField(write_only=True, required=True)
    new_password = serializers.CharField(write_only=True, required=True, validators=[validate_password])
    confirm_password = serializers.CharField(write_only=True, required=True, validators=[validate_password])

    def validate(self, data):
        password = data['password']
        new_password = data['new_password']
        confirm_password = data['confirm_password']

        user = self.context['request'].user
        if not user.check_password(password):
            raise serializers.ValidationError({'password': 'You old Password is wrong'})

        if new_password != confirm_password:
            raise serializers.ValidationError({"confirm_password": "Password didn't match"})

        return super(UserPasswordChangeSerializer, self).validate(data)

    def save(self):
        password = self.validated_data['new_password']
        user = self.context['request'].user
        user.set_password(password)
        user.mob_token = ''
        user.save()
        return user
