from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from users.serializers import UserLoginSerializer, UserDetailSerializer, UserRegisterSerializer, \
    UserPasswordChangeSerializer


class UserLogin(APIView):
    def post(self, request):
        serializer = UserLoginSerializer(data=request.data)
        if serializer.is_valid():
            validate_data = serializer.validated_data

            data = {
                'token': validate_data.get('token'),
            }

            return Response(data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CurrentUser(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        data = UserDetailSerializer(request.user).data

        return Response(data)


class Logout(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        user = request.user
        if user:
            user.mob_token = None
            user.save()
        return Response('Logged Out Successfully')


class UserRegisterView(APIView):

    def post(self, request):
        serializer = UserRegisterSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()

            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChangePasswordView(APIView):
    permission_classes = [IsAuthenticated]

    def put(self, request):
        serializer = UserPasswordChangeSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response({'detail': 'Password Changed'})

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
