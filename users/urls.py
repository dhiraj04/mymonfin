from django.urls import path
from users.views import CurrentUser, UserLogin, Logout, UserRegisterView, ChangePasswordView

app_name = 'users'

urlpatterns = [
    path('login/auth/', UserLogin.as_view(), name='login'),
    path('me/', CurrentUser.as_view(), name='current_user'),
    path('logout/', Logout.as_view(), name='logout'),
    path('register/', UserRegisterView.as_view(), name='register'),

    path('change-password/', ChangePasswordView.as_view(), name='change_password')
]
