from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db.models import Sum
from model_utils import Choices

from users.validators import UsernameValidator

USER_ROLES = Choices(
    'admin',
    'consumer',
)


class UserManager(BaseUserManager):
    def create_user(self, email, password=None, **kwargs):
        if not email:
            raise ValueError("Please enter a valid Email Address")

        if not kwargs.get('username'):
            raise ValueError('Please enter a valid username')

        user = self.model(
            username=kwargs.get('username').strip(),
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, email, password, **kwargs):
        user = self.create_user(email, password, **kwargs)

        user.role = USER_ROLES.admin
        user.is_confirmed = True
        user.save()

        return user


class User(AbstractBaseUser):
    username = models.CharField(
        max_length=250,
        unique=True,
        validators=[UsernameValidator()],
        error_messages={
            'unique': 'username already taken',
        },
    )

    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    role = models.CharField(max_length=100, choices=USER_ROLES, default=USER_ROLES.consumer)
    is_confirmed = models.BooleanField(default=True)
    mob_token = models.CharField(max_length=255, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.email

    class Meta:
        db_table = 'user'

    def get_total_amount(self):
        account = self.accounts.all()
        total_amount = account.aggregate(Sum('amount'))['amount__sum']
        return total_amount if total_amount else 0

    def is_admin(self):
        return self.role == USER_ROLES.admin

    def is_consumer(self):
        return self.role == USER_ROLES.consumer
