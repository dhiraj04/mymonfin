from django.urls import path

from money.views import IncomeListView, IncomeCreateView, IncomeEditView, ExpenseListView, ExpenseCreateView, \
    ExpenseEditView, MoneyReportView, TransactionListView

app_name = 'money'

urlpatterns = [
    path('income/', IncomeListView.as_view(), name='income'),
    path('income/add/', IncomeCreateView.as_view(), name='income_add'),
    path('income/edit/<int:pk>/', IncomeEditView.as_view(), name='income_edit'),

    path('expense/', ExpenseListView.as_view(), name='expense'),
    path('expense/add/', ExpenseCreateView.as_view(), name='expense_add'),
    path('expense/edit/<int:pk>/', ExpenseEditView.as_view(), name='expense_edit'),

    path('report/', MoneyReportView.as_view(), name='report'),


    path('transactions/', TransactionListView.as_view(), name='transactions_list')
]
