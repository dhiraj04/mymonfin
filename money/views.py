import calendar
from datetime import date, datetime
from pprint import pprint

from django.db.models import Sum
from django.http import JsonResponse, HttpResponse
from rest_framework import generics, status, serializers
from rest_framework.exceptions import NotFound
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from money.models import Income, Expense, Transaction
from money.serializers import IncomeListSerializer, IncomeCreateSerializer, IncomeEditSerializer, ExpenseListSerializer, \
    ExpenseCreateSerializer, ExpenseEditSerializer, IncomeDetailSerializer, ExpenseDetailSerializer, \
    TransactionListSerializer


class TransactionListView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        transactions = Transaction.objects.filter(user=self.request.user).order_by('-id')
        return transactions

    def get_serializer_class(self):
        return TransactionListSerializer

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class IncomeListView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        income = Income.objects.filter(user=self.request.user).order_by('-id')
        return income

    def get_serializer_class(self):
        return IncomeListSerializer

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class IncomeCreateView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        user = request.user
        serializer = IncomeCreateSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save(user=user)

            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class IncomeEditView(APIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self, pk):
        try:
            income = Income.objects.get(pk=pk, user=self.request.user)
        except Income.DoesNotExist:
            raise NotFound(detail='Income Details Not Found')
        return income

    def get(self, request, pk):
        queryset = self.get_queryset(pk)
        serializer = IncomeDetailSerializer(queryset)
        return Response(serializer.data)

    def put(self, request, pk):
        try:
            income = Income.objects.get(pk=pk, user=request.user)
        except Income.DoesNotExist:
            raise serializers.ValidationError({"Income": "Income Not Found"})
        serializer = IncomeEditSerializer(instance=income, data=request.data, partial=True, context={'request': request, 'income': income})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ExpenseListView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        income = Expense.objects.filter(user=self.request.user).order_by('-id')
        return income

    def get_serializer_class(self):
        return ExpenseListSerializer

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ExpenseCreateView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        user = request.user
        serializer = ExpenseCreateSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save(user=user)

            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ExpenseEditView(APIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self, pk):
        try:
            expense = Expense.objects.get(pk=pk, user=self.request.user)
        except Expense.DoesNotExist:
            raise NotFound(detail='Expense Details Not Found')
        return expense

    def get(self, request, pk):
        queryset = self.get_queryset(pk)
        serializer = ExpenseDetailSerializer(queryset)
        return Response(serializer.data)

    def put(self, request, pk):
        try:
            expense = Expense.objects.get(pk=pk, user=request.user)
        except Expense.DoesNotExist:
            raise serializers.ValidationError({"Expense": "Expense Not Found"})
        serializer = ExpenseEditSerializer(instance=expense, data=request.data, partial=True, context={'request': request, 'expense': expense})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MoneyReportView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        labels = []
        income_data = []
        expense_data = []

        today = datetime.now()
        current_year = today.year

        transaction_income = Transaction.objects.filter(user=request.user, transaction_date__year=current_year).values('transaction_date__month').annotate(
            total=Sum('income__income_amount'))
        transaction_expense = Transaction.objects.filter(user=request.user, transaction_date__year=current_year).values('transaction_date__month').annotate(
            total=Sum('expense__expense_amount'))

        for income in transaction_income:
            month_name = calendar.month_name[income['transaction_date__month']]
            labels.append('{},{}'.format(month_name, current_year))
            income_data.append(income['total'])

        for expense in transaction_expense:
            expense_data.append(expense['total'])

        return JsonResponse({
            'labels': labels,
            'income_data': income_data,
            'expense_data': expense_data,
        })

        # User Agent Detection
        # For more search Django User Agent Detection in Google
        # uu = request.user_agent.browser.family
        # if uu == 'Chrome Mobile':
        #    print()
        #    pprint('this is {}'.format(uu))
        #    print()
        # else:
        #    print()
        #    pprint('this is {}'.format(uu))
        #    print()
