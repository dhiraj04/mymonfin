from django.db import transaction
from rest_framework import serializers

from accounts.models import Account
from accounts.serializers import AccountDetailSerializer
from money.models import Income, Expense, Transaction


class IncomeDetailSerializer(serializers.ModelSerializer):
    account = AccountDetailSerializer()

    class Meta:
        model = Income
        fields = [
            'id',
            'income_amount',
            'remarks',
            'account',
            'income_date'
        ]


class ExpenseDetailSerializer(serializers.ModelSerializer):
    account = AccountDetailSerializer()

    class Meta:
        model = Expense
        fields = [
            'id',
            'expense_amount',
            'remarks',
            'account',
            'expense_date'

        ]


class TransactionListSerializer(serializers.ModelSerializer):
    income = IncomeDetailSerializer()
    expense = ExpenseDetailSerializer()

    class Meta:
        model = Transaction
        fields = [
            'id',
            'income',
            'expense',
            'transaction_date',
            'transactions_type',
        ]

    def get_account(self, obj):
        return obj.account.account_name


class TransactionDetailSerializer(serializers.ModelSerializer):
    account = AccountDetailSerializer()

    class Meta:
        model = Income
        fields = [
            'id',
            'income',
            'expense',
            'transaction_date',
            'transaction_type'
        ]


class IncomeListSerializer(serializers.ModelSerializer):
    account = serializers.SerializerMethodField()

    class Meta:
        model = Income
        fields = [
            'id',
            'income_amount',
            'remarks',
            'income_date',
            'account',
        ]

    def get_account(self, obj):
        return obj.account.account_name


class IncomeCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Income
        fields = [
            'income_amount',
            'remarks',
            'income_date',
            'account'
        ]

    def validate(self, data):
        user = self.context['request'].user
        user_account = Account.objects.filter(user=user)
        account_data = data['account']
        if account_data not in user_account:
            raise serializers.ValidationError({'Account': 'Account Not Found'})
        return super(IncomeCreateSerializer, self).validate(data)

    @transaction.atomic
    def create(self, validated_data):
        income_amount = Income.objects.create(**validated_data)

        account_amount = Account.objects.get(pk=income_amount.account.pk)
        account_amount.amount += income_amount.income_amount
        account_amount.save()

        transaction_income = Transaction(),
        transaction_income.income = income_amount
        transaction_income.transactions_type = 'Income'
        transaction_income.transaction_date = income_amount.income_date
        transaction_income.user = income_amount.user
        transaction_income.save()

        return income_amount


class IncomeEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = Income
        fields = [
            'income_amount',
            'remarks',
            'income_date',
            'account'
        ]

    def validate(self, data):
        user = self.context['request'].user
        user_account = Account.objects.filter(user=user)
        account_data = data['account']
        if account_data not in user_account:
            raise serializers.ValidationError({'Account': 'Account Not Found'})
        return super(IncomeEditSerializer, self).validate(data)

    @transaction.atomic
    def update(self, instance, validated_data):
        current_income = self.context['income']
        current_income_amount = current_income.income_amount

        instance.income_amount = validated_data.get('income_amount', instance.income_amount)
        instance.remarks = validated_data.get('remarks', instance.remarks)
        instance.income_date = validated_data.get('income_date', instance.income_date)
        instance.account = validated_data.get('account', instance.account)
        instance.save()

        account_amount = Account.objects.get(pk=instance.account.pk)
        current_account_amount = account_amount.amount

        previous_account_amount = current_account_amount - current_income_amount

        account_amount.amount = previous_account_amount + instance.income_amount
        account_amount.save()

        transaction_income_edit = Transaction.objects.get(income=instance)
        transaction_income_edit.transaction_date = instance.income_date
        transaction_income_edit.save()
        return instance


class ExpenseListSerializer(serializers.ModelSerializer):
    account = serializers.SerializerMethodField()

    class Meta:
        model = Expense
        fields = [
            'id',
            'expense_amount',
            'remarks',
            'expense_date',
            'account',
        ]

    def get_account(self, obj):
        return obj.account.account_name


class ExpenseCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Expense
        fields = [
            'expense_amount',
            'remarks',
            'expense_date',
            'account'
        ]

    def validate(self, data):
        user = self.context['request'].user
        user_account = Account.objects.filter(user=user)
        expense_amount = data['expense_amount']
        account_data = data['account']
        if account_data not in user_account:
            raise serializers.ValidationError({'Account': 'Account Not Found'})

        selected_account = Account.objects.get(pk=account_data.pk)
        account_amount = selected_account.amount
        if not account_amount >= expense_amount:
            raise serializers.ValidationError({'Amount': 'No Sufficient fund in the account'})

        return super(ExpenseCreateSerializer, self).validate(data)

    @transaction.atomic
    def create(self, validated_data):
        expense_amount = Expense.objects.create(**validated_data)

        account_selected = Account.objects.get(pk=expense_amount.account.pk)
        account_selected.amount -= expense_amount.expense_amount
        account_selected.save()

        transaction_expense = Transaction()
        transaction_expense.expense = expense_amount
        transaction_expense.transactions_type = 'Expense'
        transaction_expense.transaction_date = expense_amount.expense_date
        transaction_expense.user = expense_amount.user
        transaction_expense.save()

        return expense_amount


class ExpenseEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = Expense
        fields = [
            'expense_amount',
            'remarks',
            'expense_date',
            'account'
        ]

    def validate(self, data):
        current_expense = self.context['expense']
        current_expense_amount = current_expense.expense_amount

        user = self.context['request'].user
        user_account = Account.objects.filter(user=user)
        account_data = data['account']
        edit_expense_amount = data['expense_amount']
        if account_data not in user_account:
            raise serializers.ValidationError({'Account': 'Account Not Found'})

        selected_account = Account.objects.get(pk=account_data.pk)
        selected_account_current_amount = selected_account.amount
        selected_account_previous_amount = selected_account_current_amount + current_expense_amount

        if not selected_account_previous_amount >= edit_expense_amount:
            raise serializers.ValidationError({'Amount': 'No Sufficient fund in the account'})

        return super(ExpenseEditSerializer, self).validate(data)

    @transaction.atomic
    def update(self, instance, validated_data):
        current_expense = self.context['expense']
        current_expense_amount = current_expense.expense_amount

        instance.expense_amount = validated_data.get('expense_amount', instance.expense_amount)
        instance.remarks = validated_data.get('remarks', instance.remarks)
        instance.expense_date = validated_data.get('expense_date', instance.expense_date)
        instance.account = validated_data.get('account', instance.account)
        instance.save()

        selected_account = Account.objects.get(pk=instance.account.pk)
        selected_account_current_amount = selected_account.amount

        selected_account_previous_amount = selected_account_current_amount + current_expense_amount

        selected_account.amount = selected_account_previous_amount - instance.expense_amount
        selected_account.save()

        transaction_expense_edit = Transaction.objects.get(expense=instance)
        transaction_expense_edit.transaction_date = instance.expense_date
        transaction_expense_edit.save()

        return instance
