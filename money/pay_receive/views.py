from django.http import HttpResponse
from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics, status, serializers
from rest_framework.exceptions import NotFound
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from money.models import PayableReceivableGroup, PayReceiveAmount, PayReceiveTransaction
from money.pay_receive.filters import PayReceiveAmountFilter
from money.pay_receive.serializers import PayReceiveGroupListSerializer, PayReceiveGroupCreateSerializer, \
    PayReceiveGroupDetailSerializer, PayReceiveGroupEditSerializer, PayReceiveAmountListSerializer, \
    PayReceiveAmountCreateSerializer, PayReceiveAmountDetailSerializer, PayReceiveAmountEditSerializer, \
    PayReceiveTransactionsList, PayReceiveTransactionsCreateSerializer
from mymonfin.pagination import CustomPagination
from mymonfin.utils import render_pdf, create_excel_report


class PayReceiveGroupListView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        pay_receive_group = PayableReceivableGroup.objects.filter(user=self.request.user).order_by('-id')
        return pay_receive_group

    def get_serializer_class(self):
        return PayReceiveGroupListSerializer

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        # pdf = request.query_params.get('pdf')

        # if pdf == 'pdf':
        #     return render_pdf(template_name='accounts/group_pdf.html', context={'queryset': queryset}, filename='report.pdf')
        return Response(serializer.data)


class PayableReceivableGroupCreateView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        user = self.request.user
        serializer = PayReceiveGroupCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(user=user)

            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PayableReceivableGroupEditView(APIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self, pk):
        try:
            pay_receive_group = PayableReceivableGroup.objects.get(pk=pk)
        except PayableReceivableGroup.DoesNotExist:
            raise NotFound(detail='Pay Receive group Not Found')
        return pay_receive_group

    def get(self, request, pk):
        queryset = self.get_queryset(pk)
        serializer = PayReceiveGroupDetailSerializer(queryset)
        return Response(serializer.data)

    def put(self, request, pk):
        try:
            pay_receive_group = PayableReceivableGroup.objects.get(pk=pk, user=self.request.user)
        except PayableReceivableGroup.DoesNotExist:
            raise serializers.ValidationError({"detail": "Pay Receive group Not Found"})
        serializer = PayReceiveGroupEditSerializer(instance=pay_receive_group, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PayReceiveAmountListView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    filter_backends = (DjangoFilterBackend,)
    filter_class = PayReceiveAmountFilter
    pagination_class = CustomPagination

    def get_queryset(self):
        pay_receive_amount = PayReceiveAmount.objects.filter(user=self.request.user).order_by('-id')
        return pay_receive_amount

    def get_serializer_class(self):
        return PayReceiveAmountListSerializer

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        report = request.query_params.get('report')
        if page:
            if report == 'pdf':
                return render_pdf(template_name='pay_receive/pay-receive-pdf.html', context={'queryset': page},
                                  filename='report.pdf')
            elif report == 'excel':
                response = HttpResponse(content_type='application/vnd.ms-excel')
                response['Content-Disposition'] = 'attachment; filename={}'.format('report.xlsx')
                xlsx_data = create_excel_report(page)
                response.write(xlsx_data)
                return response
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        else:
            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)

    # def gets(self, request):
    #     queryset = self.filter_queryset(self.get_queryset())
    #     serializer = self.get_serializer(queryset, many=True)
    #     report = request.query_params.get('report')
    #     if report == 'pdf':
    #         return render_pdf(template_name='accounts/group_pdf.html', context={'queryset': queryset}, filename='report.pdf')
    #     elif report == 'excel':
    #         response = HttpResponse(content_type='application/vnd.ms-excel')
    #         response['Content-Disposition'] = 'attachment; filename={}'.format('report.xlsx')
    #         xlsx_data = create_excel_report(queryset)
    #         response.write(xlsx_data)
    #         return response
    #     return Response(serializer.data)


class PayReceiveAmountCreateView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        user = request.user
        serializer = PayReceiveAmountCreateSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save(user=user)

            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PayReceiveAmountEditView(APIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self, pk):
        try:
            pay_receive = PayReceiveAmount.objects.get(pk=pk, user=self.request.user)
        except PayReceiveAmount.DoesNotExist:
            raise NotFound(detail='Pay Receive Details Not Found')
        return pay_receive

    def get(self, request, pk):
        queryset = self.get_queryset(pk)
        serializer = PayReceiveAmountDetailSerializer(queryset)
        return Response(serializer.data)

    def put(self, request, pk):
        try:
            pay_receive = PayReceiveAmount.objects.get(pk=pk, user=request.user)
        except PayReceiveAmount.DoesNotExist:
            raise serializers.ValidationError({"detail": "Pay Receive Not Found"})
        serializer = PayReceiveAmountEditSerializer(instance=pay_receive, data=request.data, partial=True, context={'request': request, 'pay_receive': pay_receive})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PayReceiveTransactionsListView(generics.GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        pay_receive_transaction = PayReceiveTransaction.objects.filter(user=self.request.user).order_by('-id')
        return pay_receive_transaction

    def get_serializer_class(self):
        return PayReceiveTransactionsList

    def get(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class PayReceiveTransactionsCreateView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        user = request.user
        serializer = PayReceiveTransactionsCreateSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            serializer.save(user=user)

            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
