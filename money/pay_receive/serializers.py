from django.db import transaction
from rest_framework import serializers

from money.models import PayableReceivableGroup, PayReceiveAmount, PAY_RECEIVE, PayReceiveTransaction


class PayReceiveGroupListSerializer(serializers.ModelSerializer):
    total_amount = serializers.SerializerMethodField()

    class Meta:
        model = PayableReceivableGroup
        fields = [
            'id',
            'name',
            'total_amount'
        ]

    def get_total_amount(self, obj):
        return obj.get_total_amount()


class PayReceiveGroupDetailSerializer(serializers.ModelSerializer):
    total_amount = serializers.SerializerMethodField()

    class Meta:
        model = PayableReceivableGroup
        fields = [
            'id',
            'name',
            'total_amount'
        ]

    def get_total_amount(self, obj):
        return obj.get_total_amount()


class PayReceiveGroupCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = PayableReceivableGroup
        fields = [
            'id',
            'name',
        ]

    def create(self, validated_data):
        pay_receive_group = PayableReceivableGroup.objects.create(**validated_data)
        pay_receive_group.save()

        return pay_receive_group


class PayReceiveGroupEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = PayableReceivableGroup
        fields = [
            'id',
            'name'
        ]


class PayReceiveAmountListSerializer(serializers.ModelSerializer):
    group = PayReceiveGroupDetailSerializer()

    class Meta:
        model = PayReceiveAmount
        fields = [
            'id',
            'group',
            'amount',
            'remarks',
            'pay_receive'
        ]


class PayReceiveAmountDetailSerializer(serializers.ModelSerializer):
    group = PayReceiveGroupDetailSerializer()

    class Meta:
        model = PayReceiveAmount
        fields = [
            'id',
            'group',
            'amount',
            'remarks',
            'pay_receive'
        ]


class PayReceiveAmountCreateSerializer(serializers.ModelSerializer):
    pay_receive = serializers.ChoiceField(choices=PAY_RECEIVE)

    class Meta:
        model = PayReceiveAmount
        fields = [
            'id',
            'group',
            'amount',
            'remarks',
            'pay_receive'
        ]

    def validate(self, data):
        user = self.context['request'].user
        user_pay_receive_group = PayableReceivableGroup.objects.filter(user=user)
        pay_receive_group_data = data['group']
        if pay_receive_group_data not in user_pay_receive_group:
            raise serializers.ValidationError({'detail': 'Pay Receive Group Not Found'})
        return super(PayReceiveAmountCreateSerializer, self).validate(data)

    @transaction.atomic
    def create(self, validated_data):
        pay_receive_amount = PayReceiveAmount.objects.create(**validated_data)
        pay_receive_amount.save()

        return pay_receive_amount


class PayReceiveAmountEditSerializer(serializers.ModelSerializer):
    pay_receive = serializers.ChoiceField(choices=PAY_RECEIVE)

    class Meta:
        model = PayReceiveAmount
        fields = [
            'id',
            'group',
            'amount',
            'remarks',
            'pay_receive'
        ]

    def validate(self, data):
        user = self.context['request'].user
        user_pay_receive_group = PayableReceivableGroup.objects.filter(user=user)
        pay_receive_group_data = data['group']
        if pay_receive_group_data not in user_pay_receive_group:
            raise serializers.ValidationError({'detail': 'Pay Receive Not Found'})
        return super(PayReceiveAmountEditSerializer, self).validate(data)

    @transaction.atomic
    def update(self, instance, validated_data):
        current_pay_receive = self.context['pay_receive']
        current_pay_receive_amount = current_pay_receive.amount

        instance.amount = validated_data.get('amount', instance.amount)
        instance.remarks = validated_data.get('remarks', instance.remarks)
        instance.group = validated_data.get('income_date', instance.group)
        instance.pay_receive = validated_data.get('account', instance.pay_receive)
        instance.save()

        return instance


class PayReceiveTransactionsList(serializers.ModelSerializer):
    pay_receive_group = PayReceiveGroupDetailSerializer()

    class Meta:
        model = PayReceiveTransaction
        fields = [
            'id',
            'pay_receive_group',
            'amount',
            'remarks',
            'transaction_type',
            'transaction_date',
        ]


class PayReceiveTransactionsCreateSerializer(serializers.ModelSerializer):
    transaction_type = serializers.ChoiceField(choices=PAY_RECEIVE)

    class Meta:
        model = PayReceiveTransaction
        fields = [
            'id',
            'pay_receive_group',
            'amount',
            'remarks',
            'transaction_type',
            'transaction_date'
        ]

    def validate(self, data):
        user = self.context['request'].user
        selected_pay_receive_group = data['pay_receive_group']
        selected_transaction_type = data['transaction_type']
        entered_amount = data['amount']
        pay_receive_group = PayableReceivableGroup.objects.get(pk=selected_pay_receive_group.pk, user=user)

        total_pay_receive_amount = pay_receive_group.get_total_amount()

        if selected_transaction_type == 'Pay':
            if not total_pay_receive_amount['remain_payable']:
                raise serializers.ValidationError({'transaction_type': 'No Remaining Payable Amount Found'})

            if entered_amount > total_pay_receive_amount['remain_payable']:
                raise serializers.ValidationError({'amount': 'Amount Cannot be greater then total Remain Paying Amount'})
            elif entered_amount <= 0:
                raise serializers.ValidationError({'amount': 'Amount must be greater then 0'})

        elif selected_transaction_type == 'Receive':
            if not total_pay_receive_amount['remain_receivable']:
                raise serializers.ValidationError({'transaction_type': 'No Remaining Receivable Amount Found'})

            if entered_amount > total_pay_receive_amount['remain_receivable']:
                raise serializers.ValidationError({'amount': 'Amount Cannot be greater then total remain Receiving Amount'})
            elif entered_amount <= 0:
                raise serializers.ValidationError({'amount': 'Amount must be greater then 0'})

        return super(PayReceiveTransactionsCreateSerializer, self).validate(data)

    def create(self, validated_data):
        pay_receive_transaction = PayReceiveTransaction.objects.create(**validated_data)

        return pay_receive_transaction


class PayReceiveTransactionsEditSerializer(serializers.ModelSerializer):

    class Meta:
        model = PayReceiveTransaction
        fields = [
            'id',
            'pay_receive_group',
            'amount',
            'remarks',
            'transaction_type',
            'transaction_date'
        ]
