import django_filters
from money.models import PayReceiveAmount


class PayReceiveAmountFilter(django_filters.FilterSet):

    group__name = django_filters.CharFilter(
        lookup_expr='icontains'
    )

    class Meta:
        model = PayReceiveAmount
        fields = [
            'group',
        ]
