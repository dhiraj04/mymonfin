from django.urls import path

from money.pay_receive.views import PayReceiveGroupListView, PayableReceivableGroupCreateView, \
    PayableReceivableGroupEditView, PayReceiveAmountListView, PayReceiveAmountCreateView, PayReceiveAmountEditView, \
    PayReceiveTransactionsListView, PayReceiveTransactionsCreateView

app_name = 'pay_receive'

urlpatterns = [
    path('', PayReceiveGroupListView.as_view(), name='pay_receive_group_list'),
    path('add/', PayableReceivableGroupCreateView.as_view(), name='pay_receive_group_create'),
    path('edit/<int:pk>/', PayableReceivableGroupEditView.as_view(), name='pay_receive_group_edit'),

    path('amount/', PayReceiveAmountListView.as_view(), name='pay_receive_amount_list'),
    path('amount/add/', PayReceiveAmountCreateView.as_view(), name='pay_receive_amount_create'),
    path('amount/edit/<int:pk>/', PayReceiveAmountEditView.as_view(), name='pay_receive_amount_edit'),

    path('transactions/', PayReceiveTransactionsListView.as_view(), name='pay_receive_transactions_list'),
    path('transactions/add/', PayReceiveTransactionsCreateView.as_view(), name='pay_receive_transactions_create')
]
