import datetime

from django.db import models
from django.db.models import Sum
from model_utils import Choices

from accounts.models import Account
from users.models import User

INCOME_EXPENSE = Choices(
    'Income',
    'Expense'
)


class Transaction(models.Model):
    income = models.ForeignKey('Income', on_delete=models.CASCADE, related_name='transactions', null=True, blank=True)
    expense = models.ForeignKey('Expense', on_delete=models.CASCADE, related_name='transactions', null=True, blank=True)
    transactions_type = models.CharField(max_length=7, choices=INCOME_EXPENSE)
    transaction_date = models.DateField(default=datetime.date.today)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='transactions')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'transactions'


class Income(models.Model):
    income_amount = models.DecimalField(max_digits=15, decimal_places=2, default=0.00)
    remarks = models.CharField(max_length=255, null=True, blank=True)
    income_date = models.DateField(default=datetime.date.today)
    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='incomes')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='incomes')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'money_income'

    def __str__(self):
        return str(self.income_amount)


class Expense(models.Model):
    expense_amount = models.DecimalField(max_digits=15, decimal_places=2, default=0.00)
    remarks = models.CharField(max_length=255, null=True, blank=True)
    expense_date = models.DateField(default=datetime.date.today)
    account = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='expenses')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='expenses')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'money_expense'

    def __str__(self):
        return str(self.expense_amount)


class PayableReceivableGroup(models.Model):
    name = models.CharField(max_length=255)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='pay_receive_groups')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'pay_receive_group'

    def __str__(self):
        return self.name

    def get_total_amount(self):
        pay_amount = self.pay_receive_amounts.filter(pay_receive='Pay').aggregate(pay_total=Sum('amount'))
        receive_amount = self.pay_receive_amounts.filter(pay_receive='Receive').aggregate(receive_total=Sum('amount'))
        paid_amount = self.pay_receive_transactions.filter(transaction_type='Pay').aggregate(paid_total=Sum('amount'))
        received_amount = self.pay_receive_transactions.filter(transaction_type='Receive').aggregate(received_total=Sum('amount'))

        if paid_amount['paid_total']:
            remain_payable = pay_amount['pay_total'] - paid_amount['paid_total']
        else:
            remain_payable = pay_amount['pay_total']

        if received_amount['received_total']:
            remain_receivable = receive_amount['receive_total'] - received_amount['received_total']
        else:
            remain_receivable = receive_amount['receive_total']

        return {'pay_amount': pay_amount['pay_total'],
                'receive_amount': receive_amount['receive_total'],
                'paid_amount': paid_amount['paid_total'],
                'received_amount': received_amount['received_total'],

                'remain_payable': remain_payable,
                'remain_receivable': remain_receivable
                }


PAY_RECEIVE = Choices(
    'Pay',
    'Receive'
)


class PayReceiveAmount(models.Model):
    group = models.ForeignKey(PayableReceivableGroup, on_delete=models.CASCADE, related_name='pay_receive_amounts')
    amount = models.DecimalField(max_digits=15, decimal_places=2, default=0.00)
    remarks = models.CharField(max_length=255, null=True, blank=True)
    pay_receive = models.CharField(max_length=10, choices=PAY_RECEIVE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='pay_receive_amounts')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'pay_receive_amount'


class PayReceiveTransaction(models.Model):
    pay_receive_group = models.ForeignKey(PayableReceivableGroup, on_delete=models.CASCADE, related_name='pay_receive_transactions')
    amount = models.DecimalField(max_digits=15, decimal_places=2, default=0.00)
    remarks = models.CharField(max_length=255, null=True, blank=True)
    transaction_type = models.CharField(max_length=10, choices=PAY_RECEIVE)
    transaction_date = models.DateField(default=datetime.date.today)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='pay_receive_transactions')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'pay_receive_transactions'
