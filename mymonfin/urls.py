from django.contrib import admin
from django.urls import path, include

from pages.views import home

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home, name='home'),
    path('api/user/', include('users.urls')),
    path('api/consumers/', include('consumers.urls')),
    path('api/accounts/', include('accounts.urls')),
    path('api/money/', include('money.urls')),
    path('api/pay-receive/', include('money.pay_receive.urls')),
]
