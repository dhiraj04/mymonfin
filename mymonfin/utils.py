import io
import sys

import pdfkit
import xlsxwriter
from django.http import HttpResponse
from django.template.loader import render_to_string


def render_pdf(template_name, context, filename, options=None, attachment=True, http_response=True, path=None):
    if http_response:
        response = HttpResponse(content_type='application/pdf')
        if attachment:
            response['Content-Disposition'] = 'attachment; filename="' + filename + '"'
        else:
            response['Content-Disposition'] = 'filename="' + filename + '"'

    # return render(None, template_name, context)
    html_string = render_to_string(template_name, context)

    if options:
        options['quiet'] = ''
        options['encoding'] = 'UTF-8'
        if sys.platform == 'darwin':
            options['zoom'] = 12.5
    else:
        options = {
            'quiet': '',
            'page-size': 'A4',
            'orientation': 'Portrait',
            'margin-top': '0.5in',
            'margin-right': '0.5in',
            'margin-bottom': '0.5in',
            'margin-left': '0.5in',
            'encoding': 'UTF-8',
        }
        if sys.platform == 'darwin':
            options['zoom'] = 12.5

    if http_response:
        pdf = pdfkit.from_string(html_string, None, options=options)
        response.write(pdf)
        return response
    else:
        pdfkit.from_string(html_string, output_path=path, options=options)


def create_excel_report(queryset):

    output = io.BytesIO()

    workbook = xlsxwriter.Workbook(output, {'remove_timezone': True})
    worksheet = workbook.add_worksheet('PayReceive')

    title1 = workbook.add_format({
        'font_size': 18,
        'align': 'center',
        'valign': 'center'
    })

    cell_bold = workbook.add_format({
        'bold': True,
        'align': 'center',
        'border': 1
    })

    cell = workbook.add_format({
        'align': 'center',
        'border': 1
    })

    cell_left = workbook.add_format({
        'align': 'left',
        'border': 1
    })

    currency = workbook.add_format({
        'num_format': '#,##,##0.00',
        'align': 'left',
        'border': 1
    })

    worksheet.set_row(0, 25)
    worksheet.merge_range('A1:P1', 'Pay Receive List', title1)

    columns = ['S.N', 'Person', 'Remarks', 'Pay', 'Receive']
    for col in range(len(columns)):
        worksheet.write(4, col, columns[col], cell_bold)

    worksheet.set_column('A:A', 6)
    worksheet.set_column('B:B', 15)
    worksheet.set_column('C:C', 50)
    worksheet.set_column('D:E', 15)
    # worksheet.set_column('E:E', 12)

    row = 5  # data starts from fifth row
    count = 1
    for query in queryset:
        worksheet.write(row, 0, count, cell)
        worksheet.write(row, 1, query.group.name, cell_left)
        worksheet.write(row, 2, query.remarks, cell_left)
        if query.pay_receive == 'Pay':
            worksheet.write(row, 3, query.amount, currency)
        else:
            worksheet.write(row, 3, '-', cell_left)

        if query.pay_receive == 'Receive':
            worksheet.write(row, 4, query.amount, currency)
        else:
            worksheet.write(row, 4, '-', cell_left)

        row += 1
        count += 1

    workbook.close()
    pr = output.getvalue()
    return pr
